package com.igor.menu;

@FunctionalInterface
public interface Printable {
    public void print();
}
