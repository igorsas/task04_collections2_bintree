package com.igor.menu;

@FunctionalInterface
public interface Functional {
    void start();
}
