package com.igor.menu;

import com.igor.map.SimpleBinaryTreeMap;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class Menu{
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Map<Integer, Integer> map;
    private Scanner reader = new Scanner(System.in);

    public Menu(Map<Integer, Integer> map){
        this.map = map;

        menu = new LinkedHashMap<String, String>();
        methodsMenu = new LinkedHashMap<String, Printable>();

        menu.put("1", "1 - put new Element in the Map");
        menu.put("2", "2 - remove element by key from the Map");
        menu.put("3", "3 - get element by key from the Map");
        menu.put("4", "4 - print all element from the Map");
        menu.put("Q", "Q - exit");

        methodsMenu.put("1", this::put);
        methodsMenu.put("2", this::remove);
        methodsMenu.put("3", this::get);
        methodsMenu.put("4", this::print);
    }

    private void outputMenu(){
        System.out.println("\nMENU:");
        for (String s : menu.values()){
            System.out.println(s);
        }

    }

    private void put(){
        int key, value;
        System.out.println("Input key for adding in the map");
        try {
            key = reader.nextInt();
        }catch (Exception e){
            System.out.println("You're enter wrong key");
            return;
        }
        System.out.println("Input value:");
        try{
            value = reader.nextInt();
        }catch (Exception e){
            System.out.println("You're enter wrong value");
            return;
        }
        map.put(key, value);
    }

    private void remove(){
        System.out.println("Input key for deleting from map");
        try{
            int key = reader.nextInt();
            map.remove(key);
        }catch (Exception e){
            System.out.println("You're enter wrong key");
        }
    }

    private void get(){
        System.out.println("Input key for getting value from map");
        try{
            int key = reader.nextInt();
            System.out.println("Your value: " + map.get(key));
        }catch (Exception e){
            System.out.println("You're enter wrong key");
        }
    }

    private void print(){
        System.out.println("Your map:");
        map.forEach((a, b) -> System.out.println("Key: " + a + " Value: " + b));
    }

    public void show(){
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Choose point");
            keyMenu = reader.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){}
        }while (!keyMenu.equals("Q"));
    }
    public static void main(String[] args) {
        SimpleBinaryTreeMap<Integer, Integer> map = new SimpleBinaryTreeMap<>();
        Menu menu = new Menu(map);
        menu.show();
    }

}
